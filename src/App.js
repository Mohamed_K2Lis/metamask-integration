import React, { useEffect, useState } from "react";

import { useNavigate, useHistory } from "react-router-dom";

import { shortenAddress } from "./components/utils/shortenAddress";

const { ethereum } = window;

const networks = {
  polygon: {
    chainId: `0x${Number(137).toString(16)}`,
    chainName: "Polygon Mainnet",
    nativeCurrency: {
      name: "MATIC",
      symbol: "MATIC",
      decimals: 18,
    },
    rpcUrls: ["https://polygon-rpc.com/"],
    blockExplorerUrls: ["https://polygonscan.com/"],
  },
};

const changeNetwork = async ({ networkName, setError }) => {
  try {
    if (!ethereum) throw new Error("No crypto wallet found");
    await ethereum.request({
      method: "wallet_addEthereumChain",
      params: [
        {
          ...networks[networkName],
        },
      ],
    });
  } catch (err) {
    setError(err.message);
  }
};

const App = () => {
  const [currentAccount, setCurrentAccount] = useState("");

  const [error, setError] = useState();

  const [isConnecting, setIsConnecting] = useState(false);

  const [provider, setProvider] = useState(ethereum);

  const navigate = useNavigate();
  // let history = useHistory();

  const checkIfWalletConnected = async () => {
    try {
      if (!ethereum) return alert("Please install metamask");

      // Get our metamask connected account
      const accounts = await ethereum.request({ method: "eth_accounts" });

      console.log(accounts);

      // We can check if there is an account!
      if (accounts.length) {
        // That way at the start to every single render we gonna have access to our account!
        setCurrentAccount(accounts[0]);
      } else {
        console.log("No accounts found!");
      }
    } catch (error) {
      console.log(error);

      throw new Error("No ethereum object.");
    }
  };

  const connectWallet = async () => {
    localStorage.setItem("loggedOut", "false");

    try {
      if (!ethereum) return alert("Please install metamask");

      // Loading...
      setIsConnecting(true);

      // Get all the accounts and the user will be able here to choose and connect one!
      const accounts = await ethereum.request({
        method: "eth_requestAccounts",
      });

      // Loading...
      setIsConnecting(false);
      setCurrentAccount(accounts[0]);

      // ===========
      ethereum.on("accountsChanged", function (accounts) {
        console.log("accountChaned", accounts);
        setCurrentAccount(accounts[0]);
      });
    } catch (error) {
      console.log(error);

      throw new Error("No ethereum object.");
    }
  };

  // Control current Network via the API exposed by Metamask! (A nice user experience perspective)
  // If for example the user hasn't the BSC Mainnet in his wallet, then it will ask him
  // to add it inside his wallet
  const handleNetworkSwitch = async (networkName) => {
    setError();
    await changeNetwork({ networkName, setError });
  };
  const handleNetworkDisconnect = async () => {
    // ===========
    // const isConnected = ethereum.isConnected(
    //   "isConnected",
    //   function (accounts) {
    //     console.log("isConnected", accounts);
    //   }
    // );
    // console.log("isConnected: ", isConnected);
    const disconnect = ethereum.on("disconnect", function (accounts) {
      console.log("disconnect", accounts);
    });
    console.log("disconnect: ", disconnect);
    // ===========

    setCurrentAccount("");
    localStorage.setItem("loggedOut", "true");
    navigate("/");
  };

  const networkChanged = (chainId) => {
    window.location.reload();
    console.log({ chainId });
    // Test Avalanche Network
    // if (chainId === '0xa86a') {
    //   setIsAvalanche(true)
    // } else {
    //   setIsAvalanche(false)
    // }
  };

  const detectProvider = () => {
    let provider;
    if (window.ethereum) {
      provider = window.ethereum;
    } else if (window.web3) {
      provider = window.web3.currentProvider;
    } else {
      console.warn("No Ethereum browser detected! Check out Metamask");
    }
    //console.log('provider: ', provider.networkVersion)
    return provider;
  };

  useEffect(() => {
    // Detect Provider
    if (localStorage.getItem("loggedOut") == "true") return;

    setProvider(detectProvider());

    if (
      (localStorage.getItem("loggedOut") == "true" ||
        localStorage.getItem("loggedOut") == null) &&
      window.location.pathname != "/"
    ) {
      navigate("/");
    }

    checkIfWalletConnected();

    try {
      ethereum.on("chainChanged", networkChanged);
    } catch (e) {
      console.error(e);
    }

    return () => {
      ethereum.removeListener("chainChanged", networkChanged);
    };
  }, []);

  // The second useEffect will run each time the provider is updated
  useEffect(() => {
    if (provider) {
      if (provider !== window.ethereum) {
        console.error(
          "Not window.ethereum provider. Do you have mutiple wallets installed ?"
        );
      }
      // If we have a provider that means that metamask is well installed
    }
  }, [provider]);

  return (
    <>
      <h1>Hiiii</h1>
      {!currentAccount ? (
        <button onClick={connectWallet}>
          {!isConnecting && "Wallet Connect"}
          {isConnecting && "Loading..."}
        </button>
      ) : (
        <>
          <button onClick={() => handleNetworkDisconnect()}>Disconnect</button>
          <br />
          <br />

          {shortenAddress(currentAccount)}
        </>
      )}
    </>
  );
};

export default App;
